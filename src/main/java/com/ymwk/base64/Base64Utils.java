package com.ymwk.base64;

import java.io.IOException;
import java.util.Base64;



public class Base64Utils {

    public static byte[] decode(String str) throws IOException{
		return Base64.getDecoder().decode(str);
    }

    public static String encode(byte[] bytes){
		return new String(Base64.getEncoder().encode(bytes));
    }
    public static void main(String[] args) throws IOException {
    	String encode = encode("吴赏群wusq123".getBytes());
    	System.out.println(encode);
    	byte[] decode = decode(encode);
    	System.out.println(new String(decode));
	}
}
