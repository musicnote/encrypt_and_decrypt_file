package com.ymwk;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import com.ymwk.base64.Base64Utils;
import com.ymwk.result.Result;
import com.ymwk.sm2.SM2Util;

public class Test {
	
	public static void generateKey() throws NoSuchProviderException, NoSuchAlgorithmException, InvalidAlgorithmParameterException {
		KeyPair generateKeyPair = SM2Util.generateKeyPair();
		PrivateKey privateKey = generateKeyPair.getPrivate();
		PublicKey publicKey = generateKeyPair.getPublic();
		String privateBase = Base64Utils.encode(privateKey.getEncoded());
		String publicBase = Base64Utils.encode(publicKey.getEncoded());
		System.out.println("privateBase: "+privateBase);
		System.out.println("publicBase: "+publicBase);
		
	}
	
	
	
	public static void main(String[] args) throws CertificateException, NoSuchProviderException, IOException, NoSuchAlgorithmException, InvalidKeySpecException, InvalidAlgorithmParameterException, InvalidKeyException, InvalidCipherTextException, IllegalBlockSizeException, BadPaddingException, NoSuchPaddingException {
		 Security.addProvider(new BouncyCastleProvider());
		// FileInputStream bais = new FileInputStream("D:\\test.cer");
		// CertificateFactory cf = CertificateFactory.getInstance("X.509", BouncyCastleProvider.PROVIDER_NAME);
		// X509Certificate cert =  (X509Certificate) cf.generateCertificate(bais);
		 String orgPath="C:\\Users\\Saxon\\Desktop\\456.txt";
		 String encryptPath="C:\\Users\\Saxon\\Desktop\\123.txt";
    	 //String decryptPath="C:\\Users\\Saxon\\Desktop\\zhongk3.pdf";
		 long beforeTime = System.currentTimeMillis();
		// String cert = "MIICzzCCAnSgAwIBAgIJAdInmr8fIJiNMAoGCCqBHM9VAYN1MIGPMQswCQYDVQQGEwJDTjEzMDEGA1UECgwq5rmW5Y2X5b6h56CB572R5o6n5L+h5oGv5oqA5pyv5pyJ6ZmQ5YWs5Y+4MQ4wDAYDVQQLDAVIVU5ZTTETMBEGA1UEAwwKSFVOQU5ZTVNNMjESMBAGA1UEBwwJ6ZW/5rKZ5biCMRIwEAYDVQQIDAnmuZbljZfnnIEwHhcNMjEwNzEyMDczMjMxWhcNNDEwNzA3MDczMjMxWjCBqDELMAkGA1UEBhMCQ04xEjAQBgNVBAgMCea5luWNl+ecgTESMBAGA1UEBwwJ5bi45b635biCMTYwNAYDVQQKDC3muZbljZfnnIHmlbDlrZforqTor4HmnI3liqHkuK3lv4PmnInpmZDlhazlj7gxITAfBgNVBAsMGOW4uOW+t+W4gua1i+ivleS4k+eUqOeroDEWMBQGA1UEAwwNNDMwMzAxMDAwMzAwNDBZMBMGByqGSM49AgEGCCqBHM9VAYItA0IABOK7wKAe409JYZkd8tx6Ey6cb0OxeHZc1AG1TKrTiuM7ErQPFwPTRjkCrmnk19+xu4vZKuPR+KmYSOshoAIlm5+jgZ0wgZowHQYDVR0OBBYEFJbAkoS2vkJuuXZT+Gj0XMtEjBHwMB8GA1UdIwQYMBaAFJS9gxFrCotSpHOaUiZRiKVK6SPiMA8GA1UdEwEB/wQFMAMBAf8wDgYDVR0PAQH/BAQDAgO4MB0GA1UdJQQWMBQGCCsGAQUFBwMCBggrBgEFBQcDATAYBgNVHREEETAPgg00MzAzMDEwMDAzMDA0MAoGCCqBHM9VAYN1A0kAMEYCIQCNBSkNdrrO0ZqZC6zwpDo7ZpWwr8PqkJdge8IEBgVjbQIhAIZHM5FZY+0Bb3Kui9ZaJr5eAZb83jGXpOGSpMCQUIr6";
		// String publicKeyStr = "MIIBMzCB7AYHKoZIzj0CATCB4AIBATAsBgcqhkjOPQEBAiEA/////v////////////////////8AAAAA//////////8wRAQg/////v////////////////////8AAAAA//////////wEICjp+p6dn140TVqeS89lCafzl4n1FauPkt28vUFNlA6TBEEEMsSuLB8ZgRlfmQRGajnJlI/jC7/yZgvhcVpFiTNMdMe8Nzai9PZ3nFm9zuNraSFT0KmHfMYqR0AC3zLlITnwoAIhAP////7///////////////9yA99rIcYFK1O79Ak51UEjAgEBA0IABMWwe7cKGGDCHWVCCmI195Ump1xqhZcXn7oYa23gNPUYs8vAxmC/nf7t+8Kq7Iq2KR5zlafOR5+Igs/BX4AkUkQ=";
		 String publicKeyStr = "MIIBMzCB7AYHKoZIzj0CATCB4AIBATAsBgcqhkjOPQEBAiEA/////v////////////////////8AAAAA//////////8wRAQg/////v////////////////////8AAAAA//////////wEICjp+p6dn140TVqeS89lCafzl4n1FauPkt28vUFNlA6TBEEEMsSuLB8ZgRlfmQRGajnJlI/jC7/yZgvhcVpFiTNMdMe8Nzai9PZ3nFm9zuNraSFT0KmHfMYqR0AC3zLlITnwoAIhAP////7///////////////9yA99rIcYFK1O79Ak51UEjAgEBA0IABLsYgBDggjpSgmuWx4ZzoL+rl8xi/mv6YXILGo/po9JAzM8ar2330VIikM4YG8ttuyX9iL+V3w4EdY8E39Da1Hk=";
		 X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(Base64Utils.decode(publicKeyStr));
		 KeyFactory keyFactory = KeyFactory.getInstance("EC");
	     PublicKey publicKey = keyFactory.generatePublic(x509EncodedKeySpec);
		 // KeyPair generateKeyPair = SM2Util.generateKeyPair();
		 Result fileEncrypt = FileEncrypt.fileEncrypt(publicKey,new FileInputStream(orgPath),encryptPath);
		 System.out.println(fileEncrypt);
		 long afterTime = System.currentTimeMillis();
		 System.out.println("消耗时间:" + (afterTime-beforeTime));
		//System.out.println(DecryptFile.decrypt(generateKeyPair.getPrivate(), new File("d://test.java"), fileEncrypt.getEncryptKeyStr()));
	}
	
	
	
}
