package com.ymwk.result;

public class Result {

	public final static int CODE_SUCCESS = 200; // 正常
	public final static int CODE_ERROR = 201; // 错误
	public final static int CODE_PARAM_NULL = 202; // 参数为空
	public final static int CODE_ERROR_PARAM = 203;// 参数错误
	public final static int CODE_EXCEPTION = 500; // 发生异常
	private int code;
	private String msg;
	private String encryptDataStr;
	private String encryptKeyStr;
	

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Result() {
	}

	public String getEncryptDataStr() {
		return encryptDataStr;
	}

	public void setEncryptDataStr(String encryptDataStr) {
		this.encryptDataStr = encryptDataStr;
	}

	public String getEncryptKeyStr() {
		return encryptKeyStr;
	}

	public void setEncryptKeyStr(String encryptKeyStr) {
		this.encryptKeyStr = encryptKeyStr;
	}

	public Result(int code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	
	
	

	public Result(int code, String msg, String encryptDataStr, String encryptKeyStr) {
		super();
		this.code = code;
		this.msg = msg;
		this.encryptDataStr = encryptDataStr;
		this.encryptKeyStr = encryptKeyStr;
	}

	
	
	
	
	public Result(int code, String msg, String encryptKeyStr) {
		super();
		this.code = code;
		this.msg = msg;
		this.encryptKeyStr = encryptKeyStr;
	}

	@Override
	public String toString() {
		return "Result [code=" + code + ", msg=" + msg + ", encryptDataStr=" + encryptDataStr + ", encryptKeyStr="
				+ encryptKeyStr + "]";
	}

	
}
