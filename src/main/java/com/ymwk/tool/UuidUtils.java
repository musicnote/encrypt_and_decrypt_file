package com.ymwk.tool;

import java.util.UUID;

/**
 * 
 * @author Saxon
 * @Date   2021年7月13日
 */
public class UuidUtils {

	/**
	 * 获取UUID
	 * @return 不带-分隔符的32位UUID
	 */
	public static String uuid() {
		return UUID.randomUUID().toString().replace("-", "");
	}
	
	/**
	 * 获取UUID
	 * @return 带-分隔符的32位UUID
	 */
	public static String uuidToHG() {
		return UUID.randomUUID().toString();
	}
}
