package com.ymwk.tool;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Map;
import java.util.List;

public class ObjectUtils {

	/**
	 * 判断对象是否为空
	 * 
	 * @param obj
	 * @return
	 */
	public static boolean isEmpty(Object obj) {
		if (obj == null) {
			return true;
		}
		if (obj instanceof CharSequence) {
			return ((CharSequence) obj).length() == 0;
		}
		if (obj.getClass().isArray()) {
			return Array.getLength(obj) == 0;
		}
		if (obj instanceof Collection) {
			return ((Collection) obj).isEmpty();
		}
		if (obj instanceof Map) {
			return ((Map) obj).isEmpty();
		}
		return false;
	}

	/**
	 * 判断对象是否不为空
	 * 
	 * @param obj
	 * @return
	 */
	public static boolean isNotEmpty(Object obj) {
		return !isEmpty(obj);
	}

	/**
	 * 将List中数据拼接
	 * 
	 * @param list
	 * @return
	 */
	public static StringBuffer StringBufferAppend(List<String> list) {
		StringBuffer buffer = new StringBuffer("");
		for (int i = 0; i < list.size(); i++) {
			buffer.append(list.get(i));
		}
		return buffer;
	}
	
}
