package com.ymwk.file;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import com.ymwk.base64.Base64Utils;
import com.ymwk.tool.ObjectUtils;
import com.ymwk.tool.UuidUtils;

public class FileUtils {

	/**
	 * 获取文件名的后缀
	 * @param fileName
	 * @return
	 */
	public static String getFileNameSuffix(String fileName) {
		int lastIndexOf = fileName.lastIndexOf(".");
		if (lastIndexOf > -1) {
			String subString = fileName.substring(lastIndexOf + 1);
			return subString;
		} else {
			return "";
		}
	}

	/**
	 * 重新定义文件名
	 * @param fileName
	 * @return
	 */
	public static String renameFileName(String fileName) {
		String fileSuffix = getFileNameSuffix(fileName);
		if (ObjectUtils.isEmpty(fileSuffix)) {
			return UuidUtils.uuid();
		} else {
			return UuidUtils.uuid() + "." + fileSuffix;
		}
	}
	/**
	 * 重新定义路径
	 * @param fileName
	 * @return
	 */
	public static String generateFilePath(String filePath,String fileName) {
		String fileSuffix = getFileNameSuffix(fileName);
		if (ObjectUtils.isEmpty(fileSuffix)) {
			return filePath+File.separator+UuidUtils.uuid();
		} else {
			return filePath+File.separator+UuidUtils.uuid() + "." + fileSuffix;
		}
	}
	
	
	/***
	 * 传入文件转换成base
	 * 返回字符串
	 * @param in
	 * @return
	 */
	public static String base64InputStreamToString(InputStream in) {
        // 将图片文件转化为字节数组字符串，并对其进行Base64编码处理
        byte[] data = null;
        // 读取图片字节数组
        try {
            ByteArrayOutputStream swapStream = new ByteArrayOutputStream();
            byte[] buff = new byte[100];
            int rc = 0;
            while ((rc = in.read(buff, 0, 100)) > 0) {
                swapStream.write(buff, 0, rc);
            }
            data = swapStream.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return Base64Utils.encode(data);
    }
	
	/***
	 * 传入文件转换成base
	 * 返回字节数组
	 * @param in
	 * @return
	 */
	public static byte[] base64InputStreamToByte(InputStream in) {
        // 将图片文件转化为字节数组字符串，并对其进行Base64编码处理
        byte[] data = null;
        // 读取图片字节数组
        try {
            ByteArrayOutputStream swapStream = new ByteArrayOutputStream();
            byte[] buff = new byte[100];
            int rc = 0;
            while ((rc = in.read(buff, 0, 100)) > 0) {
                swapStream.write(buff, 0, rc);
            }
            data = swapStream.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return data;
    }
	
	/* 
	 *将InputStream写入本地文件
     * @param destination 写入本地目录
     * @param input    输入流
     * @throws IOException
     */
    public static void writeToLocal(String destination, InputStream input)
            throws IOException {
        int index;
        byte[] bytes = new byte[1024];
        FileOutputStream downloadFile = new FileOutputStream(destination);
        while ((index = input.read(bytes)) != -1) {
            downloadFile.write(bytes, 0, index);
            downloadFile.flush();
        }
        downloadFile.close();
        input.close();
    }
	
	
}
